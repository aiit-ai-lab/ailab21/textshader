#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include<stdio.h>
#include<fstream>
#include<sstream>
#include<stdlib.h>
#include<string.h>
#define _CRT_SECURE_NO_WARNINGS
const int N = 10000;

using namespace std;

#define NONE	"\033[0m"	
#define RED		"\033[31m"	
#define GREEN	"\033[32m"	
#define YELLOW	"\033[33m"	
#define BLUE	"\033[34m"	
#define MAGENTA	"\033[35m"	
#define CYAN	"\033[36m"	
#define WHITE	"\033[37m"
const string keywords[48] = {"using","namespace","define","auto","bool","break","case","char","char8_t","char16_t","char32_t","class","const","continue","default","delete","do","double","else","enum","false","float","for","friend","goto","if","int","long","new","nullptr","private","protected","public","return","short","sizeof","static","struct","switch","this","true","typedef","using","virtual","void","while","cin","cout"};
vector<string>ID;		//存放id类型（用户自定义）	WHITE
vector<string>Digit;	//数字类型					YELLOW
vector<string>KeyWords; //存放关键字类型			CYAN
vector<string>String;	//存放string类型			RED
vector<string>Char;		//存放char类型				RED
vector<string>Note;		//存放注释类型				GREEN
vector<string>Sign;		//存放特殊字符				MAGENTA
vector<string>Other;	//存放其他字符				WHITE              
#define M line << "L - " << v << "~" << row << "R：" 

int line = 1;
int row = 1;
int v;
int i = 0;  //下表为0开始
string token;  //缓存区

bool isDigit(char ch) {
	if (ch >= '0' && ch <= '9') {
		return true;
	}
	return false;
}
bool isLetter(char ch) {
	if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch == '_')) {
		return true;
	}
	return false;
}

void cover(FILE *file) {
	char ch = ' ';
	ch = fgetc(file);
	while (ch != EOF) {
		if (ch == ' ' || ch == '\t' ||ch == '\n') {
			//i++;
			row++;
			if (ch == '\n') {
				line++;
				row = 1;
			}
			ch = fgetc(file);
		}
		else {
			if (isDigit(ch)) {
				v = row;
				token = "";
				token += ch;
				ch = fgetc(file);
				row++;
				while (isDigit(ch)) {
					token += ch;
					row++;
					ch = fgetc(file);
					//i++;
				}
				if (ch == '.') {
					token += ".";
					row++;
					ch = fgetc(file);
					while (isDigit(ch)) {
						token += ch;
						row++;
						ch = fgetc(file);
						if (ch== 'e') {
							token += "e";
							row++;
							ch = fgetc(file);
							while (isDigit(ch)) {
								token += ch;
								row++;
								ch = fgetc(file);
							}
						}
					}
				}
				if (ch == 'e') {
					token += "e";
					row++;
					while (isDigit(ch = fgetc(file))) {
						token += ch;
						row++;
					}
				}
				Digit.push_back(token);
				cout << M << "digit[ " << YELLOW << token << NONE <<" ]" << endl;
			}
			else if (isLetter(ch) || ch == '_') {
				v = row;
				token = "";
				token += ch;
				row++;
				ch = fgetc(file);
				bool p = false;
				while (isLetter(ch) || isDigit(ch) || ch == '_') {
					token += ch;
					ch = fgetc(file);
					row++;
					//i++;
				}
				for (int j = 0; j < 48; j++) {
					if (token == keywords[j]) {
						KeyWords.push_back(token);
						p = true;
						cout << M << "key[ " << CYAN << token << NONE << " ]" << endl;
						break;
					}
				}
				if (p == false) {
					ID.push_back(token);
					cout << M << "id[ " << WHITE << token << NONE << " ]" << endl;
				}
			}
			else if (ch == '\"') {
				v = row;
				token = "";
				do {
					token += ch;
					ch = fgetc(file);
					row++;
					//i++;
				} while (ch != '\"');
				token += "\"";
				//i++;
				ch = fgetc(file);
				row++;
				String.push_back(token);
				cout << M << "st[ " << RED << token << NONE << " ]" << endl;
			}
			else if (ch == '\'') {
				v = row;
				token = "";
				do {
					token += ch;
					ch = fgetc(file);
					row++;
					//i++;
				} while (ch != '\'');
				token += ch;
				//i++;
				ch = fgetc(file);
				row++;
				Char.push_back(token);
				cout << M << "cha[ " << RED << token << NONE << " ]" << endl;
			}
			else {
				v = row;
				token = "";
				switch (ch) {
				case '=':
					token = "=";
					//i++;
					row++;
					ch = fgetc(file);
					if (ch == '=') {
						token = "==";
						//i++;
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else {
						row++;
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE <<" ]" << endl;
					}
					break;

				case '%':
					token += "%";
					ch = fgetc(file);
					row++;
					if (ch == '=') {
						token = "%=";
						//i++;
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else {
						row++;
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}

				case '+':
					token = "+";
					//i++;
					row++;
					ch = fgetc(file);
					row++;
					if (ch == '=') {
						token = "+=";
						//i++;
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else if (ch == '+') {
						token = "++";
						//i++;
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else {
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					break;

				case '-':
					token = "-";
					i++;
					row++;
					ch = fgetc(file);
					if (ch == '=') {
						token = "-=";
						//i++;
						ch = fgetc(file);
						row++;
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else if (ch == '-') {
						token = "--";
						//i++;
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else {
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					break;

				case '*':
					token = "*";
					//i++;
					row++;
					ch = fgetc(file);
					if (ch == '=') {
						token = "*=";
						//i++;
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else {
						row++;
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					break;

				case '/':
					token = "/";
					//i++;
					row++;
					ch = fgetc(file);
					if (ch == '=') {
						token = "/=";
						//i++;
						row++; 
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else if (ch == '/' || ch == '*') {
						int L = line;
						if (ch == '/') {
							while (ch != '\n') {
								token += ch;
								row++;
								ch = fgetc(file);
								Note.push_back(token);
							}
							cout << M << "note[ " << GREEN << token << NONE << " ]" << endl;
						}
						else {
							token += "*";
							row++;
							ch = fgetc(file);
							while (true) {
								if (ch == '*') {
									token += ch;
									row++;
									ch = fgetc(file);
									if (ch == '/') {
										token += ch;
										row++;
										ch = fgetc(file);
										break;
									}
								}
								else if (ch == '\n') {
									row = 1;
									token += ch;
									line++;
									ch = fgetc(file);
								}
								else {
									token += ch;
									row++;
									ch = fgetc(file);
								}
							}
							cout << L << "L - " << v << "R~" << line << "L - " << row << "R：" << endl << "note[ " << GREEN << token << NONE " ]" << endl;
						}
					}
					else {
						row++;
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					break;

				case '(':
					token = "(";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case ')':
					token = ")";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case '[':
					token = "[";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case ']':
					token = "]";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case '{':
					token = "{";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case '}':
					token = "}";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case ',':
					token = ",";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case ':':
					token = ":";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case ';':
					token = ";";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				case '>':
					token = ">";
					//i++;
					row++;
					ch = fgetc(file);
					if (ch == '=') {
						//i++;
						row++;
						ch = fgetc(file);
						token = ">=";
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else if (ch == '>') {
						token = ">>";
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else {
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					break;

				case '<':
					token = "<";
					//i++;
					row++;
					ch = fgetc(file);
					if (ch == '=') {
						token = "<=";
						//i++;
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else if (ch == '<') {
						token = "<<";
						row++;
						ch = fgetc(file);
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					else {
						Sign.push_back(token);
						cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					}
					break;

				case '!':
					token = "!";
					//i++;
					row++;
					ch = fgetc(file);
					Sign.push_back(token);
					cout << M << "sign[ " << MAGENTA << token << NONE << " ]" << endl;
					break;

				default:
					token = ch;
					//i++;
					row++;
					ch = fgetc(file);
					Other.push_back(token);
					cout << M << "other[ " << WHITE << token << NONE << " ]" << endl;
					break;
				}
			}
		}
	}
	
}



void read_file()
{
	int ch;
	FILE* fp;
	fp = fopen("F://project//text.txt", "r");

	while ((ch = getc(fp)) != EOF)
	{
		putc(ch, stdout);
	}
	fclose(fp);

}

void write_file()
{

	char worlds[N];
	FILE* fp1;
	fp1 = fopen("F://project//text.txt", "w+");
	if (fp1 == NULL) printf("失败");
	while ((fscanf(stdin, "%10000s", worlds) == 1))

	{

		int len = strlen(worlds);
		if (worlds[len - 1] == ' ')
		{
			fprintf(fp1, "%s", worlds);
		}
		else
			fprintf(fp1, "%s\n", worlds);
	}


	fclose(fp1);

}




int main() {
	FILE* file = fopen("F:\\project\\text.txt","r");
	cover(file);
	/*string str;
	getline(cin,str);
		do {
			cover(str);
		} while (str[i] != '\0');*/
	
	return 0;
}